db.inventory.insertMany([
		{
			"name": "JavaScript for Beginners",
			"author": "James Doe",
			"price": 5000,
			"stocks": 50,
			"publisher": "JS Publishing House"
		},
		{
			"name": "HTML and CSS",
			"author": "John Thomas",
			"price": 2500,
			"stocks": 38,
			"publisher": "NY Publishing House"
		},
		{
			"name": "Web Development Fundamentals",
			"author": "Noah Jimenez",
			"price": 3000,
			"stocks": 10,
			"publisher": "Big Idea Publishing"
		},
		{
			"name": "Java Programming",
			"author": "David Michael",
			"price": 1000,
			"stocks": 100,
			"publisher": "Big Idea Publishing"
		}
]);

// Comparison Query Operators
/*
	$gt or $gte operator (GREATER THAN)
		Syntax:
			db.collection.find({ "field": {$gt: value}})
			db.collection.find({ "field": {$gt: value}}) for gt or equal


*/
// the below results to 1
db.inventory.find({
	"stocks": {
		$gt: 50
	}})

// for $gte, results to 2
db.inventory.find({
	"stocks": {
		$gte: 50
	}})

// $lt or $lte (LESS THAN)
// the below results to 2
db.inventory.find({
	"stocks": {
		$lt: 50
	}})
// the below results to 3
db.inventory.find({
	"stocks": {
		$lte: 50
	}})

// $ne operator (NOT EQUAL)

/*
	Syntax:
			db.collection.find({ "field": {$ne: value}})

*/
// below results to 3
db.inventory.find({
	"stocks": {
		$ne: 50
	}})

// $eq operator
/*
	Syntax:
			db.collection.find({ "field": {$eq: value}})
*/
// below results to 1
db.inventory.find({
	"stocks": {
		$eq: 50
	}})

// $in operator
/*
	mentioning exact values, not range

	Syntax:
			db.collection.find({ "field": { $in: [ value1, value2] } } )
*/
db.inventory.find({
	"price": {
		$in: [1000, 5000]
	}})

// $nin operator
/*
	Syntax:
		db.collection.find({ "field": {$nin: [ value1, value2 ]}})
*/

db.inventory.find({
	"price": {
		$nin: [10000, 5000]
	}})

/* for range:
	db.inventory.find({
	"stocks": {$gt : 2000} && {$lt : 2000}})

*/

db.inventory.find({
	"price": {$lte : 4000}})

db.inventory.find({
	"price": {
		$in: [50, 100]
	}})

// Logical query operators
/*
	$or operator
		Syntax:
			db.collectionName.find({
				$or: [
						{"fieldA": "valueA"},
						{"fieldB": "valueB"}
				]
			})

*/
db.inventory.find({
				$or: [
						{"name": "HTML and CSS"},
						{"publisher": "Big Idea Publishing"}
				]
			})

db.inventory.find({
				$or: [
						{"author": "James Doe"},
						{"price": {
									$lte: 5000}}
				]
			})

// $and operator
/*
	allows to find documents with multiple criteria
		Syntax:
			db.collectionName.find({
				$and: [
						{"fieldA": "valueA"},
						{"fieldB": "valueB"}
				]
			})

*/

db.inventory.find({
				$and: [
						{"stocks": {$ne: 50}},
						{"price": {$ne: 50}}
				]
			})

// Field Projection
// Inclusion
/*
	Syntax:
		db.collectionName.find({criteria}, {field: 1}) - the field: 1 is boolean, setting if it will show or not (0 false, 1 true)

*/

db.inventory.find({
	"publisher": "JS Publishing House"
},
{
	"name": 1,
	"author": 1,
	"price": 1

})

// Exclusion
/*
	Syntax:
		db.collectionName.find({criteria}, {field: 0}) 
*/

db.inventory.find({
	"author": "Noah Jimenez"
},
{
	
	"stocks": 0,
	"price": 0

})


// "_id": 0 can work in true values
db.inventory.find({
	"price"{$lte: 5000}
},
{
	"_id": 0,
	"name": 1,
	"author": 1

}
)


// Evaluation Query Operator
// $regex operator - looking for specific string

/*
	Syntax:
		db.collectionName.find({field: { $regex: 'pattern', $options: 'optionsValue'}})
	

*/
// this shit is case sensitive
db.inventory.find({
	"author": {
		$regex: 'M'
	}
})

// for case insensitive
db.inventory.find({
	"author": {
			$regex: 'M',
			$options: '$i'
	}
})

// can query operators be used on operations such as updateOne, updateMany, deleteOne, deleteMany? YES

/*
	Sample Syntax:
		updateOne({criteria/query}, {$set})
		updateMany({criteria/query}, {$set})
		deleteOne({criteria/query})
		deleteMany({criteria/query})
*/